# Markov Decision Processes implementation
<h3>Implementation of Value Iteration, Policy Iteration and Monte Carlo Tree Search for Small, Medium and Large sized data prolems in managing investment funds for different ventures</h3>
<p>A system that uses historical profit data from the ventures, to decide how funding should be divided among an individual’s multiple ventures, so that he/she makes the most profit in the long run.</p>
