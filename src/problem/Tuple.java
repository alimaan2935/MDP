package problem;


public class Tuple implements Comparable<Tuple>{
	private LeftFund lf;
	private Allocation alloc;
	private double QValue=0.0;
	
	public Tuple(LeftFund l,Allocation a){
		this.lf=l;
		this.alloc=a;
	}
	
	public Tuple(Tuple t){
		this.lf=t.getLf();
		this.alloc=t.getAlloc();
		this.QValue=t.getQValue();
	}
	
	public double getQValue(){
		return QValue;
	}
	
	public void setQValue(double q){
		this.QValue=q;
	}
	
	public LeftFund getLf() {
		return lf;
	}

	public Allocation getAlloc() {
		return alloc;
	}
	
	
	@Override
	public int compareTo(Tuple tuple){
		if(this.QValue>tuple.getQValue()){
			return -1;
		}else if(this.QValue<tuple.getQValue()){
			return 1;
		}else{
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj){
		if(obj instanceof Tuple){
			Tuple tuple=(Tuple)obj;
			if(tuple.getAlloc().equals(this.alloc) && tuple.getLf().equals(this.lf)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		//int result=17;
		//result+=31*(lf.hashCode()+alloc.hashCode());
		int result=lf.hashCode()+alloc.hashCode()*7;
		return result;
	}
	
}
