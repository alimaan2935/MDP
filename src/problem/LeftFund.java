package problem;

import java.util.ArrayList;
import java.util.List;

public class LeftFund {
	private List<Integer> leftFundings;
	private int ventureNum;
	private double levelProfit=0.0;
	
	public LeftFund(List<Integer> leftFundings,int num){
		this.leftFundings=new ArrayList<Integer>(leftFundings);
		this.ventureNum=num;
	}
	
	public List<Integer> getLeftFundings(){
		return leftFundings;
	}
	
	public void setLevelProfit(double profit){
		this.levelProfit=profit;
	}
	public double getLevelProfit(){
		return levelProfit;
	}
	
	@Override
	public boolean equals(Object obj){
		boolean isEquals=true;
		if(obj instanceof LeftFund){
			LeftFund lf=(LeftFund)obj;
			List<Integer> funds=lf.getLeftFundings();
			if(funds.size()!=leftFundings.size()){
				System.out.println("fatal error: leftfunding lists' sizes need to be same!");
				isEquals=false;
			}else{
				for(int i=0;i<ventureNum;i++){
					if(funds.get(i)!=leftFundings.get(i)){
						isEquals=false;
						break;
					}
				}
			}
		}else{
			isEquals=false;
		}
		return isEquals;
	}
	
	@Override
	public int hashCode(){
		int result=17;
		String lf="";
		for(int i : leftFundings){
			lf+=i;
		}
		result+=31*lf.hashCode();
		return result;
	}
}
