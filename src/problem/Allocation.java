package problem;

import java.util.ArrayList;
import java.util.List;

public class Allocation {
	//possible additional funds to be allocated
	private List<Integer> allocations;
	private int ventureNum;
	
	public Allocation(int ventureNum,List<Integer> venturesAllocation){
		this.ventureNum=ventureNum;
		allocations=new ArrayList<Integer>(venturesAllocation);
	}

	public List<Integer> getVenturesAllocation() {
		return allocations;
	}

	public void setVenturesAllocation(List<Integer> venturesAllocation) {
		this.allocations =new ArrayList<Integer>(venturesAllocation);
	}

	public int getVentureNum() {
		return ventureNum;
	}

	public void setVentureNum(int ventureNum) {
		this.ventureNum = ventureNum;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof Allocation){
			Allocation alloc=(Allocation)obj;
			if(alloc.getVenturesAllocation().size()!=this.allocations.size()){
				System.out.println("fatal error: leftfunding lists' sizes need to be same!");
				return false;
			}else{
				for(int i=0;i<ventureNum;i++){
					if(alloc.getVenturesAllocation().get(i)!=this.allocations.get(i)){
						return false;
					}
				}
			}
		}else{
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode(){
		int result=17;
		String alloc="";
		for(int i : allocations){
			alloc+=i;
		}
		result+=31*alloc.hashCode();
		return result;
	}
}
