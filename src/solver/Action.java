package solver;

import java.util.List;

/**
 * Represents an Action for additional manufacturing fund for each venture.
 *
 * @author Ali
 * @author Max
 * @author Tian
 */
public class Action {

    // List of manufacturing funds for each venture
    List<Integer> action;

    /**
     * Constructor
     * @param action list of manufacturing funds for each venture
     */
    public Action(List<Integer> action) {
        this.action = action;
    }

    /**
     * @return action
     */
    public List<Integer> getAction() {
        return action;
    }
}
