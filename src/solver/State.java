package solver;

import java.util.*;

/**
 * Represents a state with valid actions.
 *
 * @author Ali
 * @author Max
 * @author Tian
 */
public class State {

    // State including current manufacturing funds for each venture
    List<Integer> state;
    // valid actions representing additional manufacturing funds for each venture
    List<Action> validActions;


    /**
     * Constructor
     * @param state list of manufacturing funds
     */
    public State(List<Integer> state) {
        this.state = state;
        validActions = new ArrayList<>();
    }

    public List<Integer> getState() {
        return state;
    }

    public List<Action> getValidActions() {
        return validActions;
    }

    public void addValidAction(Action action) {
        validActions.add(action);
    }

    /**
     * Structural equality check
     * @param o object
     * @return true if equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof State)) return false;

        State state1 = (State) o;

        for (int i = 0; i < state1.getState().size(); i++) {
            if (state1.getState().get(i) != getState().get(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return getState().hashCode();
    }
}
