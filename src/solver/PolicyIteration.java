package solver;


import problem.ProblemSpec;

import java.util.*;

/**
 * Policy iteration algorithm for solving MDP of Venture fund manager problem
 *
 * @author Ali
 * @author Max
 * @author Tian
 */
public class PolicyIteration {

    // Problem spec object
    private ProblemSpec spec;
    // Processes object to perform operations
    private Processes processes;

    // Entire state space - set of all possible states
    private List<State> stateSpace;
    // Current policy mapping every state with best action
    private Map<State, Action> policy;
    // rewards of each state
    private Map<State, Double> valueFunction;

    /**
     * Constructor
     * @param spec Problem spec
     */
    public PolicyIteration(ProblemSpec spec, Processes processes) {
        this.spec = spec;
        this.processes = processes;
        stateSpace = new ArrayList<>();
        policy = new HashMap<>();
        valueFunction = new HashMap<>();

        // Generates state space
        this.processes.generateAllStates();
        // Map all states will their possible actions
        this.processes.generateAllPossibleActions();
        // Gets the entire state space
        this.stateSpace = processes.getStateSpace();
        // Perform policy iteration algorithm
        doPolicyIteration();
    }

    /**
     * @return policy mapping states to best actions
     */
    public Map<State, Action> getPolicy() {
        return policy;
    }


    /**
     * Performs policy iteration algorithm
     */
    private void doPolicyIteration() {
        List<State> stateSpace = this.stateSpace;
        double discountFactor = spec.getDiscountFactor();
        initValueFunction();
        initPolicy();
        boolean converged = false;
        // do policy iteration until converged
        while (!converged) {
            // Update the value function using current policy
            valueFunction = calculatePolicyValue(stateSpace, discountFactor);
            // Improve Policy using updated value function
            Map<State, Action> newPolicy = improvePolicy(stateSpace, discountFactor);
            // Check for convergence
            converged = isConverged(newPolicy);
            // Update the policy
            policy = newPolicy;
        }

    }


    /**
     * Initialize policy with random action
     */
    private void initPolicy() {
        List<State> stateSpace = this.stateSpace;
        for (State s : stateSpace) {
            List<Action> validActions = s.getValidActions();
            Action randomAction = validActions.get(new Random().nextInt(validActions.size()));
            policy.put(s, randomAction);
        }
    }

    /**
     * Initialize value function with value 0
     */
    private void initValueFunction() {
        for (State s: stateSpace) {
            valueFunction.put(s, 0.0);
        }
    }

    /**
     * Calculates policy values
     * @param stateSpace entire state space
     * @param discountFactor gamma
     * @return policy mapping
     */
    private Map<State, Double> calculatePolicyValue(List<State> stateSpace, double discountFactor) {
        Map<State, Double> newValueFunction = new HashMap<>();
        for (State s : stateSpace) {
            double maxValue = Double.NEGATIVE_INFINITY;
            // Get the action for current state
            Action policyAction = policy.get(s);
            double immediateReward = processes.reward(s, policyAction);
            double expectedReward = 0;
            // Calculate expectation of future reward
            for (State sPrime : stateSpace) {
                double transitionProb = processes.transitionProb(s, policyAction, sPrime);
                double sPrimeValue = valueFunction.get(sPrime);
                expectedReward += transitionProb * sPrimeValue;
            }
            // Discount the expected reward by discount factor and then calculate overall reward
            double discountedExpectedReward = discountFactor * expectedReward;
            double value = immediateReward + discountedExpectedReward;
            // see if this is the new maximum value
            if (value > maxValue) {
                maxValue = value;
            }
            // update the value of the state in new value function
            newValueFunction.put(s, maxValue);
        }
        return newValueFunction;
    }

    /**
     * Improves policy
     * @param stateSpace entire state space
     * @param discountFactor gamma
     * @return improved policy mapping
     */
    private Map<State, Action> improvePolicy(List<State> stateSpace, double discountFactor) {
        Map<State, Action> newPolicy = new HashMap<>();
        for (State s: stateSpace) {
            // look over all possible actions in state space and update the policy
            List<Action> validActions = s.getValidActions();
            Action bestAction = null;
            double bestValue = Double.NEGATIVE_INFINITY;
            for (Action a: validActions) {
                // Calculate the value associated with using this action
                double immediateReward = processes.reward(s,a);
                double expectedReward = 0;
                // Calculate the expectation of future reward
                for (State sPrime: stateSpace) {
                    double transitionProb = processes.transitionProb(s,a,sPrime);
                    double sPrimeValue = valueFunction.get(sPrime);
                    expectedReward += transitionProb*sPrimeValue;
                }
                //Discount the expected reward by discount factor and then calculate overall reward
                double discountedExpectedReward = discountFactor * expectedReward;
                double value = immediateReward + discountedExpectedReward;
                if (value > bestValue) {
                    bestValue = value;
                    bestAction = a;
                }
            }
            // update the policy to use best action
            newPolicy.put(s, bestAction);
        }
        return newPolicy;
    }

    /**
     * Checks the convergence of policy
     * @param newPolicy new policy map
     * @return true if converged, false otherwise
     */
    private boolean isConverged(Map<State, Action> newPolicy) {
        boolean converged = true;
        List<Action> oldActions = new ArrayList<>(policy.values());
        List<Action> newActions = new ArrayList<>(newPolicy.values());
        int numActions = oldActions.size();
        // compare the old and new actions for each state
        for (int i = 0; i < numActions; i++) {
            Action oldAction = oldActions.get(i);
            Action newAction = newActions.get(i);
            if (!oldAction.equals(newAction)) {
                converged = false;
                break;
            }
        }
        return converged;
    }


}
