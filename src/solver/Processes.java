package solver;

import problem.ProblemSpec;
import problem.VentureManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Does all the common processes for solving small and large scale MDP problems
 *
 * @author Ali
 * @author Max
 * @author Tian
 */
public class Processes {

    // Problem spec object
    private ProblemSpec spec;
    // Venture manager
    private VentureManager ventureManager;

    // Entire state space - set of all possible states
    private List<State> stateSpace;

    /**
     * Constructor
     * @param spec problem spec
     * @param ventureManager venture manager object
     */
    public Processes(ProblemSpec spec, VentureManager ventureManager) {
        this.spec = spec;
        this.ventureManager = ventureManager;
        stateSpace = new ArrayList<>();
    }

    /**
     * @return entire state space
     */
    public List<State> getStateSpace() {
        return stateSpace;
    }

    /**
     * Generate entire state space
     */
    public void generateAllStates() {
        List<List<Integer>> allStates = new ArrayList<>();
        List<Integer> currentState;
        int sum;
        for (int i = 0; i <= ventureManager.getMaxManufacturingFunds(); i++) {
            for (int j = 0; j <= ventureManager.getMaxManufacturingFunds(); j++) {
                if (ventureManager.getNumVentures() > 2) {
                    for (int l = 0; l <= ventureManager.getMaxManufacturingFunds(); i++) {
                        currentState = new ArrayList<>();
                        currentState.add(i);
                        currentState.add(j);
                        currentState.add(l);
                        sum = currentState.stream().mapToInt(Integer::intValue).sum();
                        if (sum <= ventureManager.getMaxManufacturingFunds()) {
                            allStates.add(currentState);
                        }
                    }
                }else {
                    currentState = new ArrayList<>();
                    currentState.add(i);
                    currentState.add(j);
                    sum = currentState.stream().mapToInt(Integer::intValue).sum();
                    if (sum <= ventureManager.getMaxManufacturingFunds()) {
                        allStates.add(currentState);
                    }
                }
            }
        }
        for (int k = 0; k < allStates.size(); k++) {
            stateSpace.add(new State(allStates.get(k)));
        }
    }

    /**
     * Maps every state with all the possible actions from that state
     */
    public void generateAllPossibleActions() {
        for (State s: stateSpace) {
            int sum1 = s.getState().stream().mapToInt(Integer::intValue).sum();
            List<Integer> cState = s.getState();
            for (int j = 0; j < stateSpace.size(); j++) {
                List<Integer> pState = stateSpace.get(j).getState();
                List<Integer> action = new ArrayList<>();
                for (int k = 0; k < cState.size(); k++) {
                    int diff = pState.get(k) - cState.get(k);
                    if (diff >= 0) {
                        action.add(diff);
                    }else {
                        action.add(0);
                    }
                }
                int sum2 = action.stream().mapToInt(Integer::intValue).sum();
                if ((sum1+sum2) <= ventureManager.getMaxManufacturingFunds()) {
                    Action a = new Action(action);
                    s.addValidAction(a);
                }
            }
        }
    }

    /**
     * Transition probability of reaching next state given in current state and performing a particular action
     * @param current current state
     * @param action to take
     * @param sPrime next state
     * @return transition probability for that state
     */
    public double transitionProb(State current, Action action, State sPrime) {
        double transitionToReturn = 1;
        for (int i = 0; i < ventureManager.getNumVentures(); i++) {
            transitionToReturn *= doTransition(current, action, sPrime, i);
        }
        return transitionToReturn;
    }

    /**
     * Transition probability for a specific venture
     * @param current current state
     * @param action action to take
     * @param sPrime next state
     * @param ventureNumber venture index
     * @return transition probability for a particular venture
     */
    private double doTransition(State current, Action action, State sPrime, int ventureNumber) {
        double toReturn = 0;
        int currentState = current.getState().get(ventureNumber);
        int currentAction = action.getAction().get(ventureNumber);
        int nextState = sPrime.getState().get(ventureNumber);
        if (nextState > currentState + currentAction) {
            toReturn = 0;
        }else if (0 < nextState && nextState <= currentState + currentAction) {
            toReturn = spec.getProbabilities().get(ventureNumber).get(currentState+currentAction,
                    currentState+currentAction-nextState);
        }else if (nextState == 0) {
            for (int i = currentState+currentAction; i <= ventureManager.getMaxAdditionalFunding(); i++) {
                toReturn += spec.getProbabilities().get(ventureNumber).get(currentState + currentAction, i);
            }
        }
        return toReturn;
    }

    /**
     *
     * @param state
     * @param action
     * @return
     */
    public double reward(State state, Action action) {
        double rewardToReturn = 0;
        for (int i = 0; i < ventureManager.getNumVentures(); i++) {
            rewardToReturn += calculateReward(state, action, i);
        }
        return rewardToReturn;
    }

    private double calculateReward(State state, Action action, int ventureNumber) {
        double reward = 0;
        int currentState = state.getState().get(ventureNumber);
        int currentAction = action.getAction().get(ventureNumber);
        for (int i = 1; i<=ventureManager.getMaxAdditionalFunding(); i++) {
            int sale = Math.min(i, currentState+currentAction);
            reward += sale * spec.getProbabilities().get(ventureNumber).get(sale, i);
        }
        reward = 0.6 * (reward * spec.getSalePrices().get(ventureNumber));

        double penalty = 0;
        for (int j = currentState + currentAction + 1; j <= ventureManager.getMaxManufacturingFunds(); j++) {
            penalty += (j - currentState - currentAction) * spec.getProbabilities().get(ventureNumber).get(currentState+currentAction, j);
        }
        penalty = -0.25 * (penalty * spec.getSalePrices().get(ventureNumber));

        return reward + penalty;
    }

    /**
     * @return max additional funds
     */
    public int maxAdditionalFunds() {
        return ventureManager.getMaxAdditionalFunding();
    }


}
