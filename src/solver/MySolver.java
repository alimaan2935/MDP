package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import problem.VentureManager;
import problem.Allocation;
import problem.LeftFund;
import problem.Matrix;
import problem.ProblemSpec;
import problem.Simulator;
import problem.Tuple;

/**
 * This MDP solver uses 2 different methods depending upon the type of the user (Owner of ventures)
 * For Bronze and Silver venture fund owners, we use Policy Iteration
 * For Gold and Platinum venture fund owners, we use Monte Carlo Tree Search for online solutions
 *
 * @author Ali
 * @author Max
 * @author Tian
 */
public class MySolver implements FundingAllocationAgent {
	
	private ProblemSpec spec = new ProblemSpec();
	private VentureManager ventureManager;
    private List<Matrix> probabilities;
    private static double cBalance=Math.sqrt(2.0);
    private HashMap<LeftFund,List<Allocation>> allocationMap;
	private HashMap<LeftFund,Integer> leftFundTimesMap;
	private HashMap<Tuple,Integer> tupleTimesMap;
	private HashMap<Tuple,Double> tupleQValueMap;

    // For Policy Iteration
	Map<State, Action> policies;
	
	public MySolver(ProblemSpec spec) throws IOException {
	    this.spec = spec;
		ventureManager = spec.getVentureManager();
        probabilities = spec.getProbabilities();
	}
	
	public void doOfflineComputation() {
		if (ventureManager.getName().equals("bronze") || ventureManager.getName().equals("silver")) {
			Long start = System.currentTimeMillis();
			Processes pr = new Processes(spec, ventureManager);
			PolicyIteration p = new PolicyIteration(spec, pr);
			//ValueIteration p = new ValueIteration(spec, pr);
			policies = p.getPolicy();
			Long end = System.currentTimeMillis();
			System.out.println("Time taken for Policy Iteration computations: " + (end-start));
		}else {
			//Monte Carlo 
			allocationMap=new HashMap<LeftFund,List<Allocation>>();
		    leftFundTimesMap=new HashMap<LeftFund,Integer>();
		    tupleTimesMap=new HashMap<Tuple,Integer>();
		    tupleQValueMap=new HashMap<Tuple,Double>();
		}
	}
	
	public List<Integer> generateAdditionalFundingAmounts(List<Integer> manufacturingFunds,
														  int numFortnightsLeft) {

		if (ventureManager.getName().equals("bronze") || ventureManager.getName().equals("silver")) {
			return getPolicy(manufacturingFunds);
		}else {
			//Monte Carlo 
			LeftFund lf=new LeftFund(manufacturingFunds,ventureManager.getNumVentures());
			//each loop within 20 seconds
			List<Tuple> levelTuples=new ArrayList<Tuple>();
			long currentT=System.currentTimeMillis();
			long endT=currentT+25*1000;
			while(currentT<endT){
				sigleLevelSearch(lf,numFortnightsLeft);
				currentT=System.currentTimeMillis();
			}
			levelTuples=getLevelTuples(manufacturingFunds,allocationMap,tupleQValueMap);
			if(levelTuples!=null){
				Tuple bestT=getBestAction(levelTuples);
				return bestT.getAlloc().getVenturesAllocation();
			}else{
				//should not happen!
				return new ArrayList<Integer>();
			}
		}
	}
	
	//policy iteration
	private List<Integer> getPolicy(List<Integer> currentState) {
		State s = new State(currentState);
		for (State state: policies.keySet()) {
			if (state.equals(s)) {
				return policies.get(state).getAction();
			}
		}
		return currentState;
	} 
	
	/**if map does not have these tuples, estimate a Q-value with random for 15 fortnight
	 * if already has these tuples, select the max one to call simulator again*/
	private double sigleLevelSearch(LeftFund lf,int numFortnightsLeft){
		if(numFortnightsLeft<=0){
			return 0;
		}
		List<Allocation> additionalAllocations;
		if(leftFundTimesMap.get(lf)==null){
			leftFundTimesMap.put(lf, 1);
			// Get additional funding allocations
			additionalAllocations = getPossibleAllocations(lf.getLeftFundings(),false);
			allocationMap.put(lf,additionalAllocations);	
		}else{
			// Get additional funding allocations
			additionalAllocations=allocationMap.get(lf);
		}
		
		List<Tuple> levelTuples=new ArrayList<Tuple>();
		List<Tuple> newTuples=new ArrayList<Tuple>();
		boolean isContainsAll=true;//whether all tuples appeared before
		for(Allocation a : additionalAllocations){
			Tuple t=new Tuple(lf,a);
			if(tupleQValueMap.get(t)!=null){
				t.setQValue(tupleQValueMap.get(t));
				levelTuples.add(t);
			}else{
				newTuples.add(t);
				isContainsAll=false; 
			}
		}
		//Recursion to update Q-Value
		if(!isContainsAll){
			for(Tuple t : newTuples){
				double totalestQ=0.0;
				//each action will be estimated 3 times
				for(int c=0;c<3;c++){
					totalestQ+=estimateQ(t,numFortnightsLeft);
				}
				double estQ=totalestQ/3.0; //calculate average estimated Q
				t.setQValue(estQ);
				tupleQValueMap.put(t, estQ);
			}
			Collections.sort(newTuples);
			return newTuples.get(0).getQValue();
		}else{
			Tuple bestAction=getBestAction(levelTuples);
			if(bestAction==null){
				return 0; //should not happen
			}else{
				LeftFund leftFunds=randomStep(bestAction);
				double oldQ;
				if(tupleQValueMap.get(bestAction)==null){
					throw new IllegalArgumentException("QValue should not be null!");
				}else{
					oldQ=tupleQValueMap.get(bestAction);
				}
				int actVisitTimes;
				if(tupleTimesMap.get(bestAction)==null){
					actVisitTimes=1;
				}else{
					actVisitTimes=tupleTimesMap.get(bestAction);
				}
				int curFortnight=spec.getNumFortnights()-numFortnightsLeft;
				
				// transition prob
				double newQ=Math.pow(spec.getDiscountFactor(),curFortnight-1)*leftFunds.getLevelProfit()
						+sigleLevelSearch(leftFunds,numFortnightsLeft-1);
				double updateQ=((oldQ*actVisitTimes)+newQ)/(actVisitTimes+1);
				tupleQValueMap.put(bestAction, updateQ);
				tupleTimesMap.put(bestAction, actVisitTimes+1);
				bestAction.setQValue(updateQ);
				return updateQ;
			}
			
		}
		
	}
	
	//Calculate UCT to find the best action
	private Tuple getBestAction(List<Tuple> levelTuples){
		double maxUCT=0.0;
		boolean isFirst=true;
		double uct=0.0;
		Tuple bestTuple=null;
		for(Tuple t : levelTuples){
			int totalVisitTimes=leftFundTimesMap.get(t.getLf());
			if(tupleTimesMap.get(t)==null){
				//if not visit this tuple before, UCT=Q-value
				uct=t.getQValue();
			}else{
				int actionVisitTimes=tupleTimesMap.get(t);
				uct=t.getQValue()+cBalance*Math.sqrt(Math.log(totalVisitTimes)/actionVisitTimes);
			}
			if(isFirst==true){
				maxUCT=uct;
				bestTuple=new Tuple(t);
				isFirst=false;
			}else{
				if(uct>maxUCT){
					maxUCT=uct;
					bestTuple=new Tuple(t);
				}
			}
		}
		return bestTuple;
	}
	
	private List<Tuple> getLevelTuples(List<Integer> manufacturingFunds,HashMap<LeftFund,List<Allocation>> 
	                              allocationMap,HashMap<Tuple,Double> tupleQValueMap){
		List<Tuple> levelTuples=new ArrayList<Tuple>();
		LeftFund l=new LeftFund(manufacturingFunds,ventureManager.getNumVentures());
		List<Allocation> alloc=allocationMap.get(l);
		if(alloc!=null){
			for(Allocation a : alloc){
				Tuple action=new Tuple(l,a);
				if(tupleQValueMap.get(action)!=null){
					double q=tupleQValueMap.get(action);
					action.setQValue(q);
					levelTuples.add(action);
				}else{
					return null;
				}
			}
		}else{
			return null;
		}
		return levelTuples;
	}
	
	//when the action does not happen, estimate Q
	private double estimateQ(Tuple t,int numFortnightsLeft){
		int currentF=spec.getNumFortnights()-numFortnightsLeft;
		double qValue=0.0;
		Tuple tuple=new Tuple(t);
		for(int i=0;i<numFortnightsLeft;i++){
			LeftFund l=randomStep(tuple);
			Allocation randomAlloc=getPossibleAllocations(l.getLeftFundings(),true).get(0);
			tuple=new Tuple(l,randomAlloc);
			qValue+=Math.pow(spec.getDiscountFactor(), currentF-1)*l.getLevelProfit();
		}
		return qValue;
	}

	private LeftFund randomStep(Tuple t){
		List<Integer> state=new ArrayList<Integer>(ventureManager.getNumVentures());
		for(int i=0;i<ventureManager.getNumVentures();i++){
			state.add(t.getLf().getLeftFundings().get(i)+t.getAlloc().getVenturesAllocation().get(i));
		}
		List<Integer> wants=sampleCustomerOrders(state);
		double profit=0.0;
		for (int j = 0; j < wants.size(); j++) {
            // compute profit from sales
            int sold = Math.min(wants.get(j), state.get(j));
            profit += (sold * spec.getSalePrices().get(j) * 0.6);

            // compute missed opportunity penalty
            int missed = wants.get(j) - sold;
            profit -= (missed * spec.getSalePrices().get(j) * 0.25);

            // update manufacturing fund levels
            state.set(j, state.get(j) - sold);
        }
		LeftFund levelLF=new LeftFund(state,ventureManager.getNumVentures());
		levelLF.setLevelProfit(profit);	
		return levelLF;
	}
	
	//####sample orders
	public List<Integer> sampleCustomerOrders(List<Integer> state) {
		List<Integer> wants = new ArrayList<Integer>();
		for (int k = 0; k < ventureManager.getNumVentures(); k++) {
			int i = state.get(k);
			List<Double> prob = probabilities.get(k).getRow(i);
			wants.add(sampleIndex(prob));
		}
		return wants;
	}
	
	public int sampleIndex(List<Double> prob) {
		Random random = new Random();
		double sum = 0;
		double r = random.nextDouble();
		for (int i = 0; i < prob.size(); i++) {
			sum += prob.get(i);
			if (sum >= r) {
				return i;
			}
		}
		return -1;
	}
	
	
	private List<Allocation> getPossibleAllocations(List<Integer> manufacturingFunds,boolean isRandom){
		List<Allocation> additionalAllocs;
		List<Integer> ventureAllocatFunds;
		int maxManufacturingFunds=ventureManager.getMaxManufacturingFunds();
		int maxAdditionalFunds=ventureManager.getMaxAdditionalFunding();
		boolean isRandomStep=isRandom;
		int totalManufacturingFunds = 0;
		for (int i : manufacturingFunds) {
			totalManufacturingFunds += i;
		}
		
		//getAllocationFunds
		int allocationFunds=Math.min(maxManufacturingFunds-totalManufacturingFunds, 
				                                        maxAdditionalFunds);
		//Whether it is in the current step or random step
		if(!isRandomStep){
			additionalAllocs= new ArrayList<Allocation>();
			if(ventureManager.getNumVentures()==2){
				for(int i=0;i<=allocationFunds;i++){
					ventureAllocatFunds=new ArrayList<Integer>();
					ventureAllocatFunds.add(i);
					ventureAllocatFunds.add(allocationFunds-i);
					Allocation a=new Allocation(ventureManager.getNumVentures(),ventureAllocatFunds);
					additionalAllocs.add(a);
				}
			}else if(ventureManager.getNumVentures()==3){
				for(int i=0;i<=allocationFunds;i++){
					for(int j=0;j<=(allocationFunds-i);j++){
						ventureAllocatFunds=new ArrayList<Integer>();
						ventureAllocatFunds.add(i);
						ventureAllocatFunds.add(j);
						ventureAllocatFunds.add(allocationFunds-i-j);
						Allocation a=new Allocation(ventureManager.getNumVentures(),ventureAllocatFunds);
						additionalAllocs.add(a);
					}
				}
			}
			return additionalAllocs;
		}else{
			Random ra=new Random();
			additionalAllocs= new ArrayList<Allocation>();
			
			if(ventureManager.getNumVentures()==2){
				ventureAllocatFunds=new ArrayList<Integer>();
				int funds1=ra.nextInt(allocationFunds+1);
				ventureAllocatFunds.add(funds1);
				ventureAllocatFunds.add(allocationFunds-funds1);
				
				Allocation a=new Allocation(ventureManager.getNumVentures(),ventureAllocatFunds);
				additionalAllocs.add(a);
			}else if(ventureManager.getNumVentures()==3){
				ventureAllocatFunds=new ArrayList<Integer>();
				int funds1=ra.nextInt(allocationFunds+1);
				ventureAllocatFunds.add(funds1);
				int funds2=ra.nextInt(allocationFunds-funds1+1);
				ventureAllocatFunds.add(funds2);
				ventureAllocatFunds.add(allocationFunds-funds1-funds2);
				
				Allocation a=new Allocation(ventureManager.getNumVentures(),ventureAllocatFunds);
				additionalAllocs.add(a);
			}
			
			//####Valid Check
			for(Allocation a : additionalAllocs){
				if (a.getVenturesAllocation().size() != ventureManager.getNumVentures()) {
					throw new IllegalArgumentException("fatal error : Invalid additional funding list size");
				}
				int totalAdditionalFunds=0;
				int totalFunds=0;
				for(int i=0;i<a.getVenturesAllocation().size();i++){
					int addiF=a.getVenturesAllocation().get(i);
					int leftF=manufacturingFunds.get(i);
					totalAdditionalFunds+=addiF;
					totalFunds+=addiF+leftF;
				}
				if(totalAdditionalFunds>ventureManager.getMaxAdditionalFunding()){
					throw new IllegalArgumentException("Amount of additional funding is too large.");
				}
				if(totalFunds>ventureManager.getMaxManufacturingFunds()){
					throw new IllegalArgumentException("Maximum manufacturing funds exceeded.");
				}
			}
			return additionalAllocs;
		}
	}



}
