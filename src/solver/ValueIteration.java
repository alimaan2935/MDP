package solver;

import problem.ProblemSpec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Value iteration algorithm for solving MDP of Venture fund manager problem
 *
 * @author Ali
 * @author Max
 * @author Tian
 */
public class ValueIteration {

    // Max error
    private double error = 1e-7;
    // Problem spec object
    private ProblemSpec spec;
    // Processes object to perform operations
    private Processes processes;
    // Current policy mapping every state with best action
    private Map<State, Action> policy;

    // Entire state space - set of all possible states
    private List<State> stateSpace;
    // rewards of each state
    private Map<State, Double> valueFunction;

    /**
     * Constructor
     * @param spec problem spec
     * @param processes processes object to do operations
     */
    public ValueIteration(ProblemSpec spec, Processes processes) {
        this.spec = spec;
        this.processes = processes;
        this.policy = new HashMap<>();
        this.valueFunction = new HashMap<>();

        // Generates state space
        this.processes.generateAllStates();
        // Map all states will their possible actions
        this.processes.generateAllPossibleActions();
        // Gets the entire state space
        this.stateSpace = processes.getStateSpace();
        // Performs value iteration algorithm
        doValueIteration();
    }

    /**
     * @return policy mapping for state and best action
     */
    public Map<State, Action> getPolicy() {
        return policy;
    }

    /**
     * Initialize value function with value 0
     */
    private void initValueFunction() {
        for (State s: stateSpace) {
            valueFunction.put(s, 0.0);
        }
    }

    private void doValueIteration() {
        // Initialize value function
        initValueFunction();
        boolean converged = false;
        // Loop until convergence
        while (!converged) {
            // Create a new value function
            Map<State, Double> newValueFunction = new HashMap<>();
            for (State s: stateSpace) {
                // Keep track of highest value obtained
                double maxValue = Double.NEGATIVE_INFINITY;
                List<Action> validActions = s.getValidActions();
                // for all the valid actions for current state
                double maxImmediateReward = Double.NEGATIVE_INFINITY;
                double maxExpectedDiscountedReward = Double.NEGATIVE_INFINITY;
                Action bestAction = null;
                for (Action a: validActions) {
                    double immediateReward = processes.reward(s,a);
                    if (immediateReward > maxImmediateReward) {
                        maxImmediateReward = immediateReward;
                    }
                    double expectedReward = 0;
                    // Calculate the expected future reward
                    for (State sPrime: stateSpace) {
                        double transitionProbs = processes.transitionProb(s,a,sPrime);
                        double sPrimeValue = valueFunction.get(sPrime);
                        expectedReward += transitionProbs*sPrimeValue;
                    }
                    // Discount the expected reward by a discount factor and calculate overall reward
                    double discountedExpectedReward = spec.getDiscountFactor()*expectedReward;
                    if (discountedExpectedReward > maxExpectedDiscountedReward) {
                        maxExpectedDiscountedReward = discountedExpectedReward;
                    }
                    double value = immediateReward + discountedExpectedReward;
                    // Check if this is the new max reward
                    if (value > maxValue) {
                        maxValue = value;
                        bestAction = a;
                    }
                }
                // Update value of state in new value function
                newValueFunction.put(s,maxValue);
                // Update policy for best action
                policy.put(s,bestAction);
            }
            converged = isConverged(newValueFunction);
            // Update the value function
            valueFunction = newValueFunction;
        }
    }

    private boolean isConverged(Map<State, Double> newValueFunction) {
        boolean converged = true;
        List<Double> oldValues = new ArrayList<>(valueFunction.values());
        List<Double> newValues = new ArrayList<>(newValueFunction.values());
        int numberValues = oldValues.size();
        for (int i = 0; i < numberValues; i++) {
            double oldValue = oldValues.get(i);
            double newValue = newValues.get(i);
            double diff = Math.abs(oldValue - newValue);
            if (diff > this.error) {
                converged = false;
                break;
            }
        }
        return converged;
    }
}
